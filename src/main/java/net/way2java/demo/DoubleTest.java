package net.way2java.demo;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public class DoubleTest {
    public static void main(String[] args) {
        example1();
        example2();
        example3();
        example4();
        example5();
    }

    private static void example1() {
        var a = 1.1;
        var b = 1.2;
        var c = a + b;

        log.debug("example1 {}", c);

    }

    private static void example2() {
        var a = 2.1;
        var b = 2.2;
        var c = a + b;

        log.debug("example2 a {}", a);
        log.debug("example2 b {}", b);
        log.debug("example2 c {}", c);
    }

    private static void example3() {
        var b1 = new BigDecimal(2.1);
        var b2 = new BigDecimal(2.2);

        var b3 = b1.add(b2);
        log.debug("example3 b3 {}", b3);
    }

    private static void example4() {
        var b1 = BigDecimal.valueOf(2.1);
        var b2 = BigDecimal.valueOf(2.2);

        var b4 = b1.add(b2);
        log.debug("example4 b4 {}", b4);

        var b41 = BigDecimal.valueOf(2.123456789123456789);
        log.debug("example4 b41 {}", b41);
    }

    private static void example5() {
        var b1 = new BigDecimal("2.1");
        var b2 = new BigDecimal("2.2");

        var b5 = b1.add(b2);
        log.debug("example4 b5 {}", b5);

        var b51 = new BigDecimal("2.123456789123456789");
        log.debug("example4 b51 {}", b51);
    }

}


