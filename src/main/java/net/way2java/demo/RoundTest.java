package net.way2java.demo;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

// JDK에 따라 반올림 결과가 다름
@Slf4j
public class RoundTest {
    public static void main(String[] args) {
        var rnd = 0d;
        log.debug("DecimalFormat");

        var df = new DecimalFormat("#,###.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        df.setMaximumFractionDigits(2);

        rnd = 212399.535d;
        log.debug("double round {}", df.format(rnd));

        // 요부분이 JDK에 따라 결과가 다를 수 있음
        rnd = 112399.405d;
        log.debug("double round {} What the...", df.format(rnd));

        BigDecimal bd = BigDecimal.valueOf(rnd);
        var rslt = bd.setScale(2, RoundingMode.HALF_UP);

        log.debug("BigDecimal Round {}", df.format(rslt));
    }
}
